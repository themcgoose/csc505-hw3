
# themcgoose
# CSC 505 - Algorithms
# HW 3 - Coin Change
# Due: 22 Oct, 2018

import sys


def memoize(func):
    # Classic Pythonic Implementation of Memorization
    cache = {}

    # Cache Key is (value, str(denom)) becasuse
    # If denom not part of key, then multiple runs with diff denoms will results in incorrect results
    # List can't be hashed, has to be str form of list
    def inner_func(total, denom):
        if (total, str(denom)) not in cache:
            cache[(total, str(denom))] = func(total, denom)
        return cache[(total, str(denom))]
    return inner_func


@memoize  # Decorator wrapping the coin change function
def coin_change(total, denominations):
    # If reached here, then total is not in the cache, therefore must compute

    # Base Cases
    if total == 0:
        return 0
    if total in denominations:
        return 1

    # Set value to something unreasonably large
    res = 1_000_000_000_000

    # For each possible coin
    # If the coin's value is smaller or equal to total
    for value in [coin for coin in denominations if coin <= total]:
        # Consider "cashing" that coin out, and re-run
        intermediate_result = coin_change(total - value, denominations)
        # If, after cashing coin out and re-running, then the total number of coins we have to return is better
        # Then this is a move we want, so add this to our results, and continue
        if (intermediate_result + 1) < res:
            res = intermediate_result + 1
    return res


def test_coin_change():

    usd_denominations = [1, 5, 10, 25, 50]
    test_denom = [1, 3, 6, 15]

    tests = [
        {"total": 25, "denom": usd_denominations, "res": 1},
        {"total": 50, "denom": usd_denominations, "res": 1},
        {"total": 51, "denom": usd_denominations, "res": 2},
        {"total": 15, "denom": usd_denominations, "res": 2},
        {"total": 1, "denom": test_denom, "res": 1},
        {"total": 6, "denom": test_denom, "res": 1},
        {"total": 7, "denom": test_denom, "res": 2},
        {"total": 14, "denom": test_denom, "res": 4},
        # From Autogradr
        {"total": 37, "denom": [1, 5, 10, 25], "res": 4},
        {"total": 13, "denom": [1, 6, 10], "res": 3},
        {"total": 37, "denom": [1, 9, 15], "res": 5},
        {"total": 54, "denom": [1, 9, 15], "res": 4},
        {"total": 1, "denom": [1], "res":1}
    ]

    for test in tests:
        print("Calling coin_change({}, {})".format(
            test['total'], test['denom']))
        res = coin_change(test['total'], test['denom'])
        if res == test['res']:
            print("Passed")
        else:
            print("Failed.  Got {}, Expected {}".format(res, tests['res']))


def main():
    debug = True if "debug" in sys.argv[1:] else False
    test = True if "test" in sys.argv[1:] else False
    if debug:
        print("Running with Debug Output Enabled")
    if test:
        print("Skipping Input and going right to test function")
        test_coin_change()
        exit(0)

    # Read Input from Console
    # Expected Format:
    # Line 1: Total_To_Compute_Change_For Number_Of_Denominations
    # Line 2: List_Of_Denominations
    # Line 3: Returned Minimum number of coins

    total_and_num_c = input()
    denoms_input = input()

    total, num_denoms = total_and_num_c.split(" ")
    denoms = denoms_input.split(" ")

    # Type Conversion
    total = int(total)
    num_denoms = int(num_denoms)
    denoms = [int(d) for d in denoms]
    denoms.sort()  # Ensure sorted in asc order

    # Input Validation
    assert len(denoms) == num_denoms

    if debug:
        print("Got: Total {}, Num Denominations {}, Denominations {}".format(
            total, num_denoms, denoms))

    min_n_coints = coin_change(total, denoms)
    print(min_n_coints)


if __name__ == '__main__':
    main()
